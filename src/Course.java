package src;
/**
 * Lab 2 Part 3
 * @author Roy Flaherty - 21302545
 * <p>
 *  * (The Course class) Revise the Course class as follows:
 *  * ■ The client needs to be able to handle classes with more than 100 students. Do
 *       this by creating a new larger array (maybe doubling its size) every time the
 *      existing array is full.
 *  * ■ Implement the dropStudent method.
 *  * <p>
 * */
public class Course {
    /**@serialField courseName String, the name of the course*/
    private String courseName;
    /**@serialField students String[], the names of the students taking the course*/
    private String[] students = new String[4];
    private int numberOfStudents;
    /**@param courseName the name of the course to be created*/
    public Course(String courseName) {
        this.courseName = courseName;
    }
    /**@param student The student to be added to the array*/
    public void addStudent(String student) {
        if (numberOfStudents >= students.length){
            String[] temp = new String[students.length * 2];
            System.arraycopy(students, 0,temp,0,students.length);
            students = temp;
        }
        students[numberOfStudents] = student;
        numberOfStudents++;
    }
    /**@return the students from the course as an array of type String.*/
    public String[] getStudents() {
        return students;
    }
    /**@return the number of students taking the course*/
    public int getNumberOfStudents() {
        return numberOfStudents;
    }
    /**@return String gives the name of the course.*/
    public String getCourseName() {
        return courseName;
    }
    /**@param student the student to drop.
     * */
    public void dropStudent(String student) {
        for (int i=0; i < students.length; i ++){
            if (student.equalsIgnoreCase(students[i])){
                students[i] = null;
                numberOfStudents--;
                while (i < numberOfStudents) {
                    students[i] = students[i + 1];
                    i++;
                }
                break;
            }
        }
    }
}
