package src;
/*Roy Flaherty - 21302545*/
public class TestMyPoint {
    public static void main(String[] args){
        MyPoint pointOrigin = new MyPoint();
        MyPoint pointTest = new MyPoint(6, 9);
        System.out.println("Point 1: \tx: " + pointOrigin.getX() + "\ty: " + pointOrigin.getY());
        System.out.println("Point 2: \tx: " + pointTest.getX() + "\ty: " + pointTest.getY());
        System.out.println("Distance between Point 1 and Point 2:\t" + pointOrigin.distance(pointTest));
        System.out.println("Distance between Point 1 and Point x=10, y=20:\t" + pointOrigin.distance(10, 20));
        System.out.println("Distance between Point 2 and Point x=10, y=20:\t" + pointTest.distance(10, 20));
    }
}
