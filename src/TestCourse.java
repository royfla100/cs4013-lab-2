package src;
/**
 * Lab 2 Part 3
 *  * @author Roy Flaherty - 21302545
 *  */
public class TestCourse {
    public static void main (String[] args) {
        Course CS4013 = new Course("Object Oriented Programming");
        CS4013.addStudent("Roy Flaherty");
        CS4013.addStudent("Floy Raherty");
        CS4013.addStudent("Hugh Mungus");
        CS4013.dropStudent("Floy Raherty");
        String[] students = CS4013.getStudents();
        for (int i = 0; i < CS4013.getNumberOfStudents(); i++){
            System.out.print("Student #" + (i+1) + ":\t" + students[i]+"\t");
        }

    }
}
