package src;
/*Roy Flaherty - 21302545*/
public class MyPoint {
    private double x = 0;
    private double y = 0;

    public MyPoint(){
    }
    public MyPoint(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double distance(MyPoint p2){
        double p0 = Math.pow((p2.x - this.x),2)+Math.pow((p2.y - this.y),2);
        p0 = Math.sqrt(p0);
        return round(p0);
    }
    public double distance(double x2, double y2){
        double p0 = Math.pow((x2 - this.x),2)+Math.pow((y2 - this.y),2);
        p0 = Math.sqrt(p0);
        return round(p0);
    }
    /**Rounds to two decimal places*/
    public double round(double in){
        in = in * 100;
        in = Math.round(in);
        return in / 100;
    }
}
